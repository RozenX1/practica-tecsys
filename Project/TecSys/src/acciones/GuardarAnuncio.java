package acciones;

import com.opensymphony.xwork2.ActionSupport;
import org.json.JSONException;
import org.json.JSONObject;
import dominio.Gestor;

@SuppressWarnings({ "serial", "unused" })
public class GuardarAnuncio extends ActionSupport {
	private String descripcion;
	private int idCategoria;
	private int idAnunciante;
	private String pathFoto;
	private float precio;
	private String pathVideo;

	private String resultado;
	
	public String execute(){
		try{
			Gestor gestor=Gestor.get();
			gestor.guardarAnuncio(descripcion, idCategoria, idAnunciante, pathFoto, precio, pathVideo);
			this.resultado="OK";
			return SUCCESS;
		}
		catch(Exception e){
			this.resultado=e.getMessage();
			return ERROR;
		}
	}
	
	public String getResultado(){
		return this.resultado;
	}
	
	public void setDescripcion(String descripcion){
		this.descripcion=descripcion;
	}

	public void setPrecio(float precio){
		this.precio=precio;
	}

	public void setIdCategoria(int idCategoria){
		this.idCategoria=idCategoria;
	}

	public void setIdAnunciante(int idAnunciante){
		this.idAnunciante=idAnunciante;
	}

	public void setPathFoto(String pathFoto){
		this.pathFoto=pathFoto;
	}

	public void setPathVideo(String pathVideo){
		this.pathVideo=pathVideo;
	}

}

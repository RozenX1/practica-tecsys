package test;

import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
public class TestLogout {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  
  private String mail, pass, resultado;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testLogout() throws Exception {
    driver.get(baseUrl + "/TecSys/");
    Thread.sleep(2000);
    driver.findElement(By.linkText("Publianuncios")).click();
    Thread.sleep(2000);
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    
    WebElement cajaMail=driver.findElement(By.name("email"));
    WebElement cajaPass=driver.findElement(By.name("pwd1"));
    
    cajaMail.clear();
    cajaMail.sendKeys(this.mail);
    
    cajaPass.clear();
    cajaPass.sendKeys(this.pass);

    driver.findElement(By.id("btn-login")).click();
    Thread.sleep(2000);
    
    if(this.resultado.equals("OK")){
		assertEquals("Bienvenido. Ha iniciado sesi�n.", closeAlertAndGetItsText());
		Thread.sleep(2000);
		
		String textoPagina=driver.getPageSource();
    	String textoBuscado="<li id=\"logout\">";
    	assertTrue(textoPagina.indexOf(textoBuscado)!=-1);
    	
    	driver.findElement(By.linkText("Usuarios")).click();
        driver.findElement(By.linkText("Logout")).click();
        
    	Alert alert=driver.switchTo().alert();
		assertEquals(alert.getText(), "Logout.");
    } else{
		assertEquals("Usuario y contrase�a incorrectos.", closeAlertAndGetItsText());
		
		String textoPagina=driver.getPageSource();
    	String textoBuscado="<li id=\"logout\">";
    	assertTrue(textoPagina.indexOf(textoBuscado)==-1);
    }
    
  }
  
  @Parameters
  public static Collection<Object[]> valores(){
	  return Arrays.asList(new Object[][]{
		  {"hola@hola.com","hola","OK"},
		  {"nuevo@nuevo.com","nuevo","OK"},
		  {"pepe","pepe","Error"}
	  });
  }
  
  public TestLogout(String mail, String  pass, String resultado){
	  this.mail=mail;
	  this.pass=pass;
	  this.resultado=resultado;
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

package test;

import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
public class TestChat {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  
  private String mail1, pass1, mail2, pass2, mensaje, resultado;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testChat() throws Exception {
    driver.get(baseUrl + "TecSys/");
    Thread.sleep(2000);
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    
    WebElement cajaMail1=driver.findElement(By.name("email"));
    WebElement cajaPass1=driver.findElement(By.name("pwd1"));
    
    cajaMail1.clear();
    cajaMail1.sendKeys(this.mail1);
    
    cajaPass1.clear();
    cajaPass1.sendKeys(this.pass1);
    
    driver.findElement(By.id("btn-login")).click();
    Thread.sleep(2000);
    assertEquals("Bienvenido. Ha iniciado sesi�n.", closeAlertAndGetItsText());
    Thread.sleep(2000);
    driver.findElement(By.id("chat-menu")).click();
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
    driver.get(baseUrl + "TecSys/");
    Thread.sleep(2000);
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    
    WebElement cajaMail2=driver.findElement(By.name("email"));
    WebElement cajaPass2=driver.findElement(By.name("pwd1"));
    
    cajaMail2.clear();
    cajaMail2.sendKeys(this.mail2);
    
    cajaPass2.clear();
    cajaPass2.sendKeys(this.pass2);
    
    driver.findElement(By.id("btn-login")).click();
    Thread.sleep(2000);
    assertEquals("Bienvenido. Ha iniciado sesi�n.", closeAlertAndGetItsText());
    Thread.sleep(2000);
    
    driver.findElement(By.id("chat-menu")).click();
    driver.findElement(By.name("message")).clear();
    driver.findElement(By.name("message")).sendKeys(mensaje);
    driver.findElement(By.id("send-message")).click();
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t");
    Thread.sleep(2000);
    
    String textoPagina=driver.getPageSource();
	String textoBuscado=mensaje;
	assertTrue(textoPagina.indexOf(textoBuscado)!=-1);
  }
  
  @Parameters
  public static Collection<Object[]> valores(){
	  return Arrays.asList(new Object[][]{
		  {"nuevo@nuevo.com","nuevo", "hola@hola.com","hola","hola, soy hola@hola.com","OK"}
	  });
  }
  
  public TestChat(String mail1, String  pass1, String mail2, String  pass2, String mensaje, String resultado){
	  this.mail1=mail1;
	  this.pass1=pass1;
	  this.mail2=mail2;
	  this.pass2=pass2;
	  this.mensaje=mensaje;
	  this.resultado=resultado;
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

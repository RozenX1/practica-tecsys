package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestSubirAnuncio {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testSubirAnuncio() throws Exception {
    driver.get(baseUrl + "/TecSys/");
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("nuevo@nuevo.com");
    driver.findElement(By.name("pwd1")).clear();
    driver.findElement(By.name("pwd1")).sendKeys("nuevo");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.cssSelector("b")).click();
    driver.findElement(By.name("descripcion")).clear();
    driver.findElement(By.name("descripcion")).sendKeys("Manta de bebé azul por 5 €");
    driver.findElement(By.name("foto")).clear();
    driver.findElement(By.name("foto")).sendKeys("C:\\Users\\Maripi\\Desktop\\mantita.jpg");
    driver.findElement(By.id("btn-add-adv")).click();
    driver.findElement(By.linkText("Publianuncios")).click();
    driver.findElement(By.id("cat2")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

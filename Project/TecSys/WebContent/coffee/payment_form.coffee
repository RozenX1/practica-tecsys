$ ->
	window.PAYMILL_PUBLIC_KEY = '73526331872c85ebedc0b7850df1bbd0'
	
	PaymillResponseHandler = (error, result) ->
		if error
			$('.payment-errors').text error.apierror
			alert 'Error en el pago'
		else
			$('.payment-errors').text ''
			form = $ '#payment-form'
			form.append "<input type='hidden' name='paymillToken' value='#{result.token}'><>"
			alert 'Éxito en el pago'
			form.get(0).submit()
			window.location.href = '/TecSys/index.html'

		$('.submit-button').removeAttr 'disabled'

	$('#payment-form').on "submit", (e)->
		do e.preventDefault
		submit = $ '.submit-button' 
		card_number = $ '.card-number' 
		expiry_month = $ '.card-expiry-month' 
		expiry_year = $ '.card-expiry-year' 

		submit.attr "disabled", "disabled" 
		card_number_ok = paymill.validateCardNumber card_number.val()
		if not card_number_ok
			$(".payment-errors").text "Tarjeta de crédito inválida" 
			submit.removeAttr "disabled" 
			return

		expiry_ok = paymill.validateExpiry expiry_month.val(), expiry_year.val() 
		if not expiry_ok
			$(".payment-errors").text "Fecha de expiración inválida." 
			submit.removeAttr "disabled" 
			return

		paymill.createToken
			number: card_number.val()
			exp_month: expiry_month.val()
			exp_year: expiry_year.val()
			cvc: $('.card-cvc').val()
			cardholdername: $('.card-holdername').val()
		, PaymillResponseHandler


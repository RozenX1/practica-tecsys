$ ->
	# PLACES
	fill_select = (data, select) ->
		select.empty()
		for e in data.all
			select.append $("<option>",
				value: e.id,
				text: e.nombre
			)

	charge_municipio = ->
		option = $("#provincia option:selected")
		$.get "./jsp/get_location.jsp?tipo=Municipio&idPadre=#{option.val()}", (data) ->
			fill_select data, $('#municipio')

	charge_provincia = ->
		option = $("#ccaa option:selected")
		$.get "./jsp/get_location.jsp?tipo=Provincia&idPadre=#{option.val()}", (data) ->
			fill_select data, $('#provincia')
			do charge_municipio

	charge_ccaa = ->
		$.get './jsp/get_location.jsp?tipo=Comunidad%20autónoma', (data) ->
			fill_select data, $('#ccaa')
			do charge_provincia

	do charge_ccaa
	$('#ccaa').on "change", charge_provincia
	$('#provincia').on "change", charge_municipio

	# CHARGE ADV FOR CATEGORY
	adv_for_category = (id) ->
		$("#categories .active").removeClass("active")
		$("#cat#{id}").addClass("active")
		$.get "./jsp/get_adv.jsp?idCategoria=#{id}" , (data) -> 
			refresh_adv data.all

	# CATEGORIES
	$.get './jsp/get_categories.jsp', (data) ->
		categories = $("#categories")
		for cat in data.all
			new_cat = $ "<a href='#' class='list-group-item' id='cat#{cat.id}'>#{cat.nombre}</a>"
			new_cat.on "click", adv_for_category.bind(@, cat.id)
			categories.append(new_cat)

	# SHOW ADV

	html_adv = (anun, adv) ->
		has_video = anun.pathVideo? and anun.pathVideo isnt "null"
		video = "
			<video id='video#{ anun.id }' style='width:100%;height:200px' controls>
				<source src='./videos/#{ anun.pathVideo }' type='video/mp4'>
			</video>
		"
		adv.append "
			#{ if has_video then video else '' }
			<img src=./photos/#{anun.pathFoto} style='width:100%;height:200px;' id='foto#{ anun.id }'>
			<h3><span class='label label-info' style='font-size:16px;'>#{ anun.fechaDeAlta.split(" ")[0] }</span></h3>
			<p class='lead' style='padding-top:10px;'>#{ anun.descripcion }</p>
			<h3><span class='label label-success' style='font-size:16px;'>#{ anun.precio.toFixed(2) } <span class='glyphicon glyphicon-eur'></span></span></h3>
			<a class='btn btn-warning' href='./payment_form.jsp?price=#{ anun.precio.toFixed(2) }'><i><span class='glyphicon glyphicon-ok-circle'></span> Pagar con Paymill</i></a>
			<form action='https://www.paypal.com/cgi-bin/webscr' method='post' target='_top' style='margin-top:10px;> 
				<input type='hidden' name='cmd' value='_s-xclick'> 
				<input type='hidden' name='hosted_button_id' value='7A7SXYZN5FXRJ'> 
				<input type='image' src='https://www.paypalobjects.com/es_ES/ES/i/btn/btn_paynow_LG.gif' border='0' name='submit' alt='PayPal. La forma rápida y segura de pagar en Internet.''>
				<img alt='' border='0' src='https://www.paypalobjects.com/es_ES/i/scr/pixel.gif'width='1' height='1'>
			</form>
			<br>
			<div class='btn-group btn-group-sm' style='margin-bottom:10px'>
				<button class='btn btn-default'  id='toggle-foto#{ anun.id }'><span class='glyphicon glyphicon-picture'></span> <b>Foto</b></button>
				<button class='btn btn-default' id='toggle-video#{ anun.id }'><span class='glyphicon glyphicon-film'></span> <b>Video</b></button>
			</div>
		"

		if not has_video
			$("#toggle-foto#{anun.id},#toggle-video#{anun.id}").addClass('disabled') 
		else
			video = $("#video#{ anun.id }")
			foto = $("#foto#{ anun.id }")
			video.hide()

			$("#toggle-foto#{anun.id}").on "click", ->
				video.hide()
				foto.show()

			$("#toggle-video#{anun.id}").on "click", ->
				video.show()
				foto.hide()


	refresh_adv = (data) ->
			advs = $("#advertisement")
			advs.empty()
			for anun, index in data
				adv = $ "<div class='col-md-3 thumbnail text-center'><div>" 
				advs.append("<div class='row'></div>") if index % 4 is 0				
				adv.appendTo("#advertisement .row:last")
				html_adv anun, adv

			advs.append("<h3 class='text-center text-info'>No se encontraron resultados. Revise ubicación, categoría y palabras clave.</h3><br>") if data.length is 0
			return

	# FILTER ADV
	$('#search').on "click", ->
		url = "./jsp/get_search.jsp?idLocation="
		municipio = $("#municipio")
		if municipio.is(":visible") 
			url += municipio.find('option:selected').val()
		else
			provincia = $("#provincia")
			if provincia.is(":visible")
				url += provincia.find('option:selected').val()
			else
				url += $('#ccaa option:selected').val()

		$.get url, (data) ->
			data = data.all
			# Category filter
			cat = $("#categories .active")
			id = cat.attr 'id'
			if id isnt "cat-all"
				data = data.filter (a) -> "cat#{a.idCategoria}" is id
			# Keys filter
			keys = $('input[name=keys]').val().split /\s*,+\s*/
			if keys[0]
				data = data.filter (adv) ->
			        keys.reduce (res, key) -> 
			            (adv.descripcion.indexOf(key) > -1) and res
			        , true

			refresh_adv data

		# Select preference
		prefs = ["city", "animals", "food", "people", "transport", "city", "nightlife", "cats", "fashion", "sports", "nature"]
		my_pref = prefs[parseInt(Math.random()*(prefs.length-1))]

		request = new XMLHttpRequest()
		request.open "GET","http://localhost:8085/CentroDePublicidad/recibirPreferencia.jsp?preferencia=#{my_pref}" 
		request.withCredentials = yes
		request.setRequestHeader "Content-Type", "application/x-www-form-urlencoded" 
		do request.send 


	# TOGGLE PLACES
	$('#toggle-ccaa').on "click", ->
		$('#provincia').hide()
		$('#municipio').hide()

	$('#toggle-provincia').on "click", ->
		$('#provincia').show()
		$('#municipio').hide()

	$('#toggle-municipio').on "click", ->
		$('#provincia').show()
		$('#municipio').show()

	all = -> 
		$.get './jsp/get_adv.jsp', (data) ->
			$("#categories .active").removeClass("active")
			$("#cat-all").addClass("active")
			refresh_adv data.all

	$('#cat-all').on "click", all.bind(@)
	do all
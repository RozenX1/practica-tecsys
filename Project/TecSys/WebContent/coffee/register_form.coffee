$ ->
	fill_select = (data, select) ->
		select.empty()
		for e in data.all
			select.append $("<option>",
				value: e.id,
				text: e.nombre
			)

	charge_municipio = ->
		option = $("#provincia option:selected")
		$.get "./jsp/get_location.jsp?tipo=Municipio&idPadre=#{option.val()}", (data) ->
			fill_select data, $('#municipio')

	charge_provincia = ->
		option = $("#ccaa option:selected")
		$.get "./jsp/get_location.jsp?tipo=Provincia&idPadre=#{option.val()}", (data) ->
			fill_select data, $('#provincia')
			do charge_municipio

	charge_ccaa = ->
		$.get './jsp/get_location.jsp?tipo=Comunidad%20autónoma', (data) ->
			fill_select data, $('#ccaa')
			do charge_provincia

	$('#btn-register').on "click", ->
		url = "Register.action?"
		inputs = $("#form-register input").each (i,e) ->
			url += "#{$(e).attr('name')}=#{$(e).val()}&"
		ubicacion = $('select[name=municipio]').val()
		url += "ubicacion=#{ubicacion}"

		$.ajax
			method: "POST"
			contentType: "application/x-www-form-urlencoded" 
			url: url 
			success: (data) ->
				if data.resultado is "OK"
					alert "¡Registrado con éxito!"
					window.location.href = '/TecSys'
				else
					console.log "Error #{JSON.stringify data}"
					alert "¡Algo ha fallado!"
			error: (data) ->
				console.log "Error #{JSON.stringify data}"
				alert "¡Algo ha fallado!"

	do charge_ccaa
	$('#ccaa').on "change", charge_provincia
	$('#provincia').on "change", charge_municipio


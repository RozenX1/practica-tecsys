package dominio;

import java.sql.SQLException;
import dominio.Usuario;

@SuppressWarnings("unused")
public class Gestor {
	private static Gestor yo;

	private Gestor(){
		
	}
	public static Gestor get() {
		if(yo==null) yo =new Gestor();
		return yo;
	}
	public void registrar(String email, String nombre, String apellido1, String apellido2, String telefono, String pwd1,
			int ubicacion) throws Exception {
		Usuario usuario=new Usuario(email, nombre, apellido1, apellido2, telefono, pwd1, ubicacion);
		usuario.insert();
		
	}
	
	public Usuario identificar(String email, String pwd) throws Exception {
		Usuario usuario=new Usuario(email,pwd);
		return usuario;
	}
	
	public void guardarAnuncio(String descripcion, int idCategoria, int idAnunciante, String pathFoto, float precio, String pathVideo) throws Exception {
		Anuncio anuncio=new Anuncio(descripcion, idCategoria, idAnunciante, pathFoto, precio, pathVideo);
		anuncio.insert();
		
	}
	
	public void registrarUsuarioGoogle(String email) throws Exception {
		Usuario usuario=new Usuario(email);
		usuario.insert(2);
	}

}

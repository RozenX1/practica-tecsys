$ ->
	$('#btn-login').on "click", ->
		email = $('input[name=email]').val()
		pwd = $('input[name=pwd1]').val()
		url = "Identificar.action?email=#{email}&pwd=#{pwd}"
		
		$.ajax
			method: "POST"
			contentType: "application/x-www-form-urlencoded" 
			url: url 
			success: (data) ->
				if data.resultado is "OK"
					alert "Bienvenido. Ha iniciado sesión."
					sessionStorage.setItem('email', email)
					window.location.href = '/TecSys'
				else
					alert "Usuario y contraseña incorrectos."
					console.log "Error #{JSON.stringify data}"
			error: (data) ->
				alert "Usuario y contraseña incorrectos."
				console.log "Error #{JSON.stringify data}"
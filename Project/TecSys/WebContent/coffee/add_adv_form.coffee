$ ->
	# SUBMIT FORM
	$('#adv-form').on "submit", (e) ->
		e.preventDefault()
		do upload_video

	upload_adv = (video_path, photo_path) ->
		email = sessionStorage.getItem 'email'
		$.get "./jsp/get_id_user.jsp?email=#{email}", (data_get) ->
			desc = $ 'textarea[name=descripcion]'
			price = $ 'input[name=precio]'
			cat = $('select[name=categoria]')

			url = "GuardarAnuncio.action?" + $.param
				descripcion: desc.val()
				idCategoria: cat.val()
				idAnunciante: data_get.id
				pathFoto: photo_path
				precio: price.val()
				pathVideo: video_path

			$.post url, (data) ->
				alert "¡Anuncio añadido correctamente!"
				window.location.href = '/TecSys'

	upload_photo = (video_path) ->
		info = new FormData() 
		foto = $ 'input[name=foto]' 
		info.append 'upload', foto.prop('files')[0], foto.val()
		$.ajax
				url: 'SubirFoto.action'
				method: "POST"
				data : info
				processData: off
				contentType: off
				success: (data) ->
					upload_adv video_path, data.name
	
	upload_video = ->
		video = $ 'input[name=video]'
		# It's optional
		if video.val()
			info = new FormData
			info.append 'upload', video.prop('files')[0], video.val()

			$.ajax
				url: 'SubirVideo.action'
				method: "POST"
				data : info
				processData: off
				contentType: off
				success: (data) ->
					upload_photo data.name
								
		else
			upload_photo "null"


	# CATEGORIES
	$.get './jsp/get_categories.jsp', (data) ->
		select = $ 'select[name=categoria]'
		for cat in data.all
			select.append $("<option>",
				value: cat.id,
				text: cat.nombre
			)
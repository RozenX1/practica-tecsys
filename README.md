# **Publianuncios**

Este es el proyecto para la asignatura Tecnologías y Sistemas Web (2015-2016) de la Escuela Superior de Informática de Ciudad Real [ESI](http://webpub.esi.uclm.es/).

### **¿Qué es publianuncios?** ###

Es una aplicación web de anuncios, un portal donde cualquiera puede consultar aquellas ofertas que otros usuarios han patrocinado, así como registrarse y añadir las suyas propias. Puedes buscar tu temática favorita o aquellos anuncios cuyo patrocinador esté más cerca. Este proyecto utiliza las tecnologías de **Struts2**, **Twitter** **Bootstrap**, **Apache Tomcat** y **Coffeescript** entre otros.

### **Desarrollado por:** ###

* Maria del Pilar Moreno Duque
* Raúl Campos Rubio

### **Documentación** ###

Puede encontrar toda la documentación necesaria en las diferentes secciones de la [wiki del proyecto](https://bitbucket.org/RozenX1/practica-tecsys/wiki/).

![WWWW.png](https://bitbucket.org/repo/44rnB6/images/763891861-WWWW.png)
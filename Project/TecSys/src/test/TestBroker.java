package test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.junit.Test;

import dao.Broker;
import dao.Params;

public class TestBroker {

	//@Test
	public void testAperturaConexiones() throws Exception {
		Broker bd=Broker.get();
		
		int i=1, j=1;
		boolean error=false;
		do{
			try{
				bd.getSelector();
				System.out.println("Conexi�n de selecci�n " + i++);
			}
			catch(SQLException e){
				error=true;
			}
		}while(!error && i<Params.MAX_SELECT);
		
		do{
			try{
				bd.getInserter();
				System.out.println("Conexi�n de inserci�n " + j++);
			}
			catch(SQLException e){
				error=true;
			}
		}while(!error && j<Params.MAX_INSERT);
		
		assertTrue(i==Params.MAX_SELECT && j==Params.MAX_INSERT);
	}
	
	@Test
	public void testInsercion(){
		String sql="Insert into Pruebas (cadena1,cadena2) values(?,?)";
		try{
			long timeIni=System.currentTimeMillis();
			Connection bd=Broker.get().getInserter();
			PreparedStatement p=bd.prepareStatement(sql);
			for(int i=0;i<300;i++){
				String cadenaAleatoria=getCadenaAleatoria();
				p.setString(1, cadenaAleatoria);
				p.setString(2, cadenaAleatoria);
				p.executeUpdate();
			}
			bd.close();
			long timeFinal=System.currentTimeMillis();
			System.out.println("Tiempo de inserci�n: " + ((timeFinal-timeIni)/1000));
		}
		catch (Exception e){
			fail("Excepci�n en la inserci�n: "+e);
		}
	}

	
	@Test
	public void testSeleccion() {
		String sql1="Select * from Pruebas order by cadena1";
		String sql2="Select * from Pruebas order by cadena2";
		try{
			long timeIni2=System.currentTimeMillis();
			Connection bd2=Broker.get().getSelector();
			PreparedStatement p2=bd2.prepareStatement(sql2);
			ResultSet r2=p2.executeQuery();
			r2.next();
			long timeFinal2=System.currentTimeMillis();
			System.out.println("Tiempo de selecci�n sin indice: " + ((timeFinal2-timeIni2)/10));
			
			long timeIni1=System.currentTimeMillis();
			Connection bd=Broker.get().getSelector();
			PreparedStatement p=bd.prepareStatement(sql1);
			ResultSet r=p.executeQuery();
			r.next();
			long timeFinal1=System.currentTimeMillis();
			System.out.println("Tiempo de selecci�n con indice: " + ((timeFinal1-timeIni1)/10));
		}
		catch(Exception e){
			fail("Excepci�n en la selecci�n: "+e);
		}
	}
	
	private String getCadenaAleatoria() {
		String r="";
		int longitud=new Random().nextInt(255)+1;
		for(int i=0;i<longitud;i++){
			char c=(char) (new Random().nextInt(100)+65);
			r+=c;
		}
		return r;
	}
}

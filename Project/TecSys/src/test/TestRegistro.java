package test;

import java.util.regex.Pattern;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import dao.Broker;

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
public class TestRegistro {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  private String mail, nombre, pass1, pass2, apellido1, apellido2, telefono, resultado;
  
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testRegistro() throws Exception {
    driver.get(baseUrl + "/TecSys/");
    Thread.sleep(2000);
    

    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    WebElement cajaMail=driver.findElement(By.name("email"));
    WebElement cajaPass=driver.findElement(By.name("pwd1"));
    
    cajaMail.clear();
    cajaMail.sendKeys(this.mail);
    
    cajaPass.clear();
    cajaPass.sendKeys(this.pass1);
    
    driver.findElement(By.id("btn-login")).click();
    Thread.sleep(2000);
    
    assertEquals("Usuario y contrase�a incorrectos.", closeAlertAndGetItsText());
    
    driver.findElement(By.linkText("Publianuncios")).click();
    Thread.sleep(2000);
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Registrar")).click();
    Thread.sleep(2000);
    
    cajaMail=driver.findElement(By.name("email"));
    WebElement cajaNombre=driver.findElement(By.name("nombre"));
    cajaPass=driver.findElement(By.name("pwd1"));
    WebElement cajaPass2=driver.findElement(By.name("pwd2"));
    WebElement cajaApellido1=driver.findElement(By.name("apellido1"));
    WebElement cajaApellido2=driver.findElement(By.name("apellido2"));
    WebElement cajaTelefono=driver.findElement(By.name("telefono"));
    
    cajaMail.clear();
    cajaMail.sendKeys(this.mail);
    
    cajaNombre.clear();
    cajaNombre.sendKeys(this.nombre);
    
    cajaPass.clear();
    cajaPass.sendKeys(this.pass1);
    
    cajaPass2.clear();
    cajaPass2.sendKeys(this.pass2);
    
    cajaApellido1.clear();
    cajaApellido1.sendKeys(this.apellido1);
    
    cajaApellido2.clear();
    cajaApellido2.sendKeys(this.apellido2);
    
    cajaTelefono.clear();
    cajaTelefono.sendKeys(this.telefono);

    new Select(driver.findElement(By.id("ccaa"))).selectByVisibleText("Castilla-La Mancha");
    driver.findElement(By.cssSelector("option[value=\"8\"]")).click();
    new Select(driver.findElement(By.id("provincia"))).selectByVisibleText("Ciudad Real");
    driver.findElement(By.cssSelector("option[value=\"47\"]")).click();
    new Select(driver.findElement(By.id("municipio"))).selectByVisibleText("Ciudad Real");
    
    driver.findElement(By.id("btn-register")).click();
    Thread.sleep(2000);
    
    if(this.resultado.equals("OK")){
		assertEquals("�Registrado con �xito!", closeAlertAndGetItsText());
    } else{
		assertEquals("�Algo ha fallado!", closeAlertAndGetItsText());
    }
    
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    
    cajaMail=driver.findElement(By.name("email"));
    cajaPass=driver.findElement(By.name("pwd1"));
    
    cajaMail.clear();
    cajaMail.sendKeys(this.mail);
    
    cajaPass.clear();
    cajaPass.sendKeys(this.pass1);
    
    driver.findElement(By.id("btn-login")).click();
    Thread.sleep(2000);
    
    if(this.resultado.equals("OK")){
    	Alert alert=driver.switchTo().alert();
		assertEquals(alert.getText(), "Bienvenido. Ha iniciado sesi�n.");
    } else{
    	Alert alert=driver.switchTo().alert();
		assertEquals(alert.getText(), "Usuario y contrase�a incorrectos.");
    }
     
    String sql="Delete from usuarios where email=?";
	try{
		Connection bd=Broker.get().getInserter();
		PreparedStatement ps=bd.prepareStatement(sql);
		ps.setString(1, this.mail);
		ps.executeUpdate();
		bd.close();
	}
	catch(Exception e){
		fail("Excepci�n en el borrado: "+e);
	}
  }

  @Parameters
  public static Collection<Object[]> valores(){
	  return Arrays.asList(new Object[][]{
		  {"prueba@prueba.com","prueba","prueba","prueba","prueba","prueba","666666666","OK"}
	  });
  }
  
  public TestRegistro(String mail, String nombre, String  pass1, String pass2, String apellido1, String apellido2, String telefono, String resultado){
	  this.mail=mail;
	  this.nombre=nombre;
	  this.pass1=pass1;
	  this.pass2=pass2;
	  this.apellido1=apellido1;
	  this.apellido2=apellido2;
	  this.telefono=telefono;
	  this.resultado=resultado;
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

<%@ page language="java" contentType="application/json"%>
<%@ page import="java.sql.*, org.json.*, dao.*" %>
<%
	Connection bd = Broker.get().getSelector();
	String strCategoria = request.getParameter("idCategoria");

	PreparedStatement ps;
	if(strCategoria == null){
		String sql = "SELECT * FROM anuncios ORDER BY fechaDeAlta DESC";
		ps = bd.prepareStatement(sql);
	}else{
		int idCategoria = Integer.parseInt(strCategoria);
		String sql = "SELECT * FROM anuncios WHERE idCategoria=? ORDER BY fechaDeAlta DESC";
		ps = bd.prepareStatement(sql);
		ps.setInt(1, idCategoria);
	}
	ResultSet rs = ps.executeQuery();
	JSONArray jsa = new JSONArray();
	while (rs.next()) {
	   JSONObject jso = new JSONObject();
	   jso.put("id",rs.getInt(1));
	   jso.put("fechaDeAlta", rs.getTimestamp(2));
	   jso.put("descripcion", rs.getString(3));
	   jso.put("idAnunciante", rs.getInt(5));
	   jso.put("pathFoto", rs.getString(6));
	   jso.put("precio", rs.getFloat(7));
	   jso.put("pathVideo", rs.getString(8));
	   jsa.put(jso);
	}
	bd.close();
%>
{"all":
	<%= jsa.toString() %>
}



package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RegistroCorrecto {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testRegistroCorrecto() throws Exception {
    driver.get(baseUrl + "/TecSys/");
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Registrar")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("prueba@prueba.com");
    driver.findElement(By.name("nombre")).clear();
    driver.findElement(By.name("nombre")).sendKeys("prueba");
    driver.findElement(By.name("pwd1")).clear();
    driver.findElement(By.name("pwd1")).sendKeys("prueba");
    driver.findElement(By.name("pwd2")).clear();
    driver.findElement(By.name("pwd2")).sendKeys("prueba");
    driver.findElement(By.name("apellido1")).clear();
    driver.findElement(By.name("apellido1")).sendKeys("prueba");
    driver.findElement(By.name("apellido2")).clear();
    driver.findElement(By.name("apellido2")).sendKeys("prueba");
    driver.findElement(By.name("telefono")).clear();
    driver.findElement(By.name("telefono")).sendKeys("666666666");
    new Select(driver.findElement(By.id("ccaa"))).selectByVisibleText("Castilla-La Mancha");
    driver.findElement(By.cssSelector("option[value=\"8\"]")).click();
    new Select(driver.findElement(By.id("provincia"))).selectByVisibleText("Ciudad Real");
    driver.findElement(By.cssSelector("option[value=\"47\"]")).click();
    new Select(driver.findElement(By.id("municipio"))).selectByVisibleText("Ciudad Real");
    driver.findElement(By.id("btn-register")).click();
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("prueba@prueba.com");
    driver.findElement(By.name("pwd1")).clear();
    driver.findElement(By.name("pwd1")).sendKeys("prueba");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.linkText("Usuarios")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

$ ->
	# SESSION OPTIONS
	user = sessionStorage.getItem('email')
	if user
		$('#login').hide()
		$('#register').hide()
		$('#google-login').hide()
		$("#right-bar").prepend "<li><p class='navbar-text'><b>Logeado como: </b>#{ user }</p></li>"
	else
		$('#add-adv').hide()
		$('#logout').hide()
		$('#chat-popup').hide()

	$('#logout').on "click", ->
		sessionStorage.removeItem('email')
		if sessionStorage.getItem('oauth')
			sessionStorage.removeItem('oauth')
			auth2 = gapi.auth2.getAuthInstance()
			auth2.signOut().then ->
				alert "Logout de Oauth."
		else
			alert "Logout."

		document.location.href = '/TecSys'

	# GOOGLE OAUTH
	window.google_sign_in = (google_user) ->
		if not sessionStorage.getItem('email')
			email = google_user.getBasicProfile().getEmail()

			$.post "RegistrarUsuarioGoogle.action?email=#{email}", (data) ->
				sessionStorage.setItem "email", email
				sessionStorage.setItem "oauth", true

				alert "Registrado con Google+ exitosamente."
				document.location.href = '/TecSys'

	# CHAT
	$('#chat-menu').on "click", ->
		$(@).parent().toggleClass "open"

	if not WebSocket?
		$("#chat-menu").disable()
		$('#chat-popup').removeClass "open"
	else
		chat = new WebSocket "ws://#{location.hostname}:#{location.port}/TecSys/chat"
		colors = palette('tol-rainbow', 15)
		people_color = {}

		chat.onerror = ->
			$("#chat-menu").disable()
			$('#chat-popup').removeClass "open"

		chat.onmessage = (message) ->
			data = JSON.parse(message.data)
			if not people_color[data.sender]?
				total_people = Object.keys(people_color).length
				people_color[data.sender] = colors[total_people % 15]
				
			sender = if sessionStorage.getItem('email') is data.sender then 'yo' else data.sender
			$('#chat-messages').append $("<b style='color:##{people_color[data.sender]};'>#{sender}</b>: #{data.body}<br>")

		# Disable send btn when empty input
		btn_send = $('#send-message').get 0 
		btn_send.disabled = yes
		$('input[name=message]').on "keyup", ->
			if $(@).val() is ''
				btn_send.disabled = yes
			else 
 				btn_send.disabled = no 

		$('#chat-form').on "submit", (e) ->
			e.preventDefault()
			input = $('input[name=message]')
			message = 
				sender: sessionStorage.getItem 'email'
				body: input.val()

			input.val ''
			btn_send.disabled = yes
			chat.send JSON.stringify message

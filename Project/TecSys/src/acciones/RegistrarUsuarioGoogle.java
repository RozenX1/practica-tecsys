package acciones;

import org.json.JSONException;
import org.json.JSONObject;
import com.opensymphony.xwork2.ActionSupport;
import dominio.Usuario;
import dominio.Gestor;

@SuppressWarnings({ "serial", "unused" })
public class RegistrarUsuarioGoogle extends ActionSupport {
	
	private String email;
	private String resultado;
	
	public String execute(){
		try{
			Gestor gestor=Gestor.get();
			gestor.registrarUsuarioGoogle(email);
			this.resultado="OK";
			return SUCCESS;
		}
		catch(Exception e){
			this.resultado=e.getMessage();
			return ERROR;
		}
	}
	
	public String getResultado(){
		return this.resultado;
	}

	public void setEmail(String email){this.email=email;}

}

package test;

import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@RunWith(Parameterized.class)
public class TestSubirAnuncio {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  private String mail, pass, descripcion, urlFoto, resultado;
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testSubirAnuncio() throws Exception {
    driver.get(baseUrl + "/TecSys/");
    Thread.sleep(2000);
    driver.findElement(By.linkText("Usuarios")).click();
    driver.findElement(By.linkText("Login")).click();
    Thread.sleep(2000);
    
    WebElement cajaMail=driver.findElement(By.name("email"));
    WebElement cajaPass=driver.findElement(By.name("pwd1"));
    
    cajaMail.clear();
    cajaMail.sendKeys(this.mail);
    
    cajaPass.clear();
    cajaPass.sendKeys(this.pass);
    
    driver.findElement(By.id("btn-login")).click();
    assertEquals("Bienvenido. Ha iniciado sesi�n.", closeAlertAndGetItsText());
    Thread.sleep(2000);
    driver.findElement(By.cssSelector("b")).click();
    Thread.sleep(2000);
    
    WebElement cajaDescripcion=driver.findElement(By.name("descripcion"));
    WebElement cajaFoto=driver.findElement(By.name("foto"));
    
    cajaDescripcion.clear();
    cajaDescripcion.sendKeys(this.descripcion);
    
    cajaFoto.clear();
    cajaFoto.sendKeys(this.urlFoto);
    
    driver.findElement(By.id("btn-add-adv")).click();
    assertEquals("�Anuncio a�adido correctamente!", closeAlertAndGetItsText());
    Thread.sleep(2000);
    driver.findElement(By.linkText("Publianuncios")).click();
    Thread.sleep(2000);
    driver.findElement(By.id("cat2")).click();
    Thread.sleep(2000);
    
    if(this.resultado.equals("OK")){
    	String textoPagina=driver.getPageSource();
    	String textoBuscado=">"+descripcion+"</p>";
    	assertTrue(textoPagina.indexOf(textoBuscado)!=-1);
    } else{
    	String textoPagina=driver.getPageSource();
    	String textoBuscado=">"+descripcion+"</p>";
    	assertTrue(textoPagina.indexOf(textoBuscado)==-1);
    }
  }

  @Parameters
  public static Collection<Object[]> valores(){
	  return Arrays.asList(new Object[][]{
		  {"hola@hola.com","hola",getCadenaAleatoria(),"C:\\Users\\Maripi\\Desktop\\mantita.jpg","OK"}
	  });
  }
  
  public TestSubirAnuncio(String mail, String pass, String  descripcion, String urlFoto, String resultado){
	  this.mail=mail;
	  this.pass=pass;
	  this.descripcion=descripcion;
	  this.urlFoto=urlFoto;
	  this.resultado=resultado;
  }
  
  private static String getCadenaAleatoria() {
		int longitud=3;
		String desc=randomAlphabetic(longitud).toLowerCase();
		System.out.println(desc);
		return desc;
	}
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

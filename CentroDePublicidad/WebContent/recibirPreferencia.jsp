<%@ page language="java" contentType="text/plain; charset=UTF-8" pageEncoding="UTF-8"%>
<%
response.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");
response.setHeader("Access-Control-Allow-Credentials", "true");

String preferencia = request.getParameter("preferencia");
if(preferencia!=null){
	Cookie cookie = new Cookie("cookiePublicidad", preferencia);
	//cookie.setMaxAge(3600*24*10); Para 10 días
	cookie.setMaxAge(30); //Un minuto
	response.addCookie(cookie);
	%>
		{"Result":"Cookie stablished"}
	<%
}

%>